# WRG home page mockup 

## Dependencies

- [LiveReload](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei) Chrome Extension
- [node](http://nodejs.org/) `sudo apt-get install nodejs`
- [npm](http://www.npmjs.org) `curl -L https://npmjs.org/install.sh | sh`
- [grunt](http://gruntjs.com/getting-started/) `npm install -g grunt-cli`
- [bower](http://bower.io/) `npm install -g bower`

## How to Install



1. `git clone https://thinkxl@bitbucket.org/adwhite/woodlands-relo-group-home-mockup.git --recursive WRG-website`
2. `cd WRG-website`
3. `git checkout development`
4. `npm install`
5. `bower install`

## How to hack into 

1. `cd path/to/WRG-website`
2. `grunt`
3. Start editing files

## To build the production folder

1. `grunt build`
2. A `prod/` folder will be created and the website ready to upload will be there.
