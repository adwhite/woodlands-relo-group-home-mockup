(function($) {
	var $circleWithText = $('.circle-with-text'),
		$circleWithoutText = $('.circle-without-text');

	$circleWithoutText.on('mouseenter', function(e) {
		var $this = $(this);

		$this.fadeOut('fast', function() {
			$this.siblings('.circle-with-text').fadeIn();
		});
	});

	$circleWithText.on('mouseout', function(e) {
		var $this = $(this);

		$this.fadeOut('fast', function() {
			$this.siblings('.circle-without-text').fadeIn();
		});
	})

	var $mobileNav = $('.mobile-nav'),
		$desktopNav = $('.desktop-nav'),
		$displayNav = $('.display-nav');

	$displayNav.on('click', function(e) {
		e.preventDefault();

		$desktopNav.slideToggle();
	});
})(jQuery);
